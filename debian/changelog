python-ua-parser (0.18.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright year.
  * Update Standards-Version.

 -- Edward Betts <edward@4angle.com>  Fri, 22 Sep 2023 07:34:42 +0100

python-ua-parser (0.16.1-2) unstable; urgency=medium

  * Fix package to build with setuptools 65.5.0. (Closes: #1022474)

 -- Edward Betts <edward@4angle.com>  Mon, 24 Oct 2022 12:42:31 +0100

python-ua-parser (0.16.1-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Sun, 28 Aug 2022 21:15:32 +0100

python-ua-parser (0.16.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Wed, 24 Aug 2022 12:28:38 +0100

python-ua-parser (0.15.0-2) unstable; urgency=medium

  * New upstream release.
  * Update copyright year.
  * Update Standards-Version.
  * Update debian/watch to version 4.
  * Switch from dh --with python in debian/rules to a dh-sequence-python3
  * Remove egg-info during clean target.
  * Run tests when building package.
  * Add Build-Depends python3-pytest and uap-core for tests.

 -- Edward Betts <edward@4angle.com>  Wed, 29 Jun 2022 12:23:27 +0100

python-ua-parser (0.10.0-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Emmanuel Arias ]
  * Add patch to remove yaml from setup-requires.
  * d/clean: add file.
  * Add autopkgtest.
    - Reproduce #991364.
  * d/control: Add python3-yaml as depedency (Closes: #991364).
  * d/control: Bump debhelper-compat to 13.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Wed, 29 Sep 2021 16:12:00 -0300

python-ua-parser (0.10.0-1) unstable; urgency=medium

  * New upstream release.

 -- Edward Betts <edward@4angle.com>  Thu, 19 Mar 2020 19:59:29 +0000

python-ua-parser (0.9.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Edward Betts ]
  * New upstream release.
  * Update Standards-Version.
  * Set Rules-Requires-Root to no.
  * Update debhelper-compat to 12.

 -- Edward Betts <edward@4angle.com>  Mon, 10 Feb 2020 09:58:03 +0000

python-ua-parser (0.8.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add Vcs-* field

  [ Edward Betts ]
  * New upstream release.
  * d/rules: Update standards version, no changes.
  * d/rules: Set Maintainer to Debian Python Modules Team.

 -- Edward Betts <edward@4angle.com>  Sat, 05 May 2018 10:31:58 +0100

python-ua-parser (0.7.3-1) unstable; urgency=low

  * Initial release. (Closes: #890327)

 -- Edward Betts <edward@4angle.com>  Wed, 14 Feb 2018 08:00:00 +0000
